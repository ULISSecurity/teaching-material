#!/usr/bin/env python3

# PYTHONUNBUFFERED=1

import os
import signal
import random
import time
from base64 import b64encode
from Crypto.Util.number import getStrongPrime, bytes_to_long
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
from flag import flag

p = getStrongPrime(1024)

key = os.urandom(32)  # aes-256
iv = os.urandom(AES.block_size)
aes = AES.new(key=key, mode=AES.MODE_CBC, iv=iv)
c = aes.encrypt(pad(flag, AES.block_size))

key = bytes_to_long(key)
print("Encrypted flag: {}".format(b64encode(iv + c).decode()))
time.sleep(0.5)
print("p = {}".format(p))
time.sleep(0.5)
print("key.bit_length() = {}".format(key.bit_length()))

"""
[*] b'YOE5A/PLbgU0lI88u33jEhehnGo059hZpMeuBkRmp2QxbV9usTnMhALby8ohSwuV'
[*] 142085798633663171542099409878870235622173676914038477967797196374658794160724560102731675945512354963758426153785872320356361175660866436074111334394752735753511950144352933103494681440930815597469669877593910526781221331962701302376587263260062042570582685978881043667351826565667200520619448015296814703989
[*] 256
"""

signal.alarm(600)
while key > 0:
    r = random.randint(2, p - 1)
    s = random.randint(2, p - 1)

    a = int(input("a = ")) % p
    b = int(input("b = ")) % p
    c = int(input("c = ")) % p
    assert len(set([a, b, c])) == 3

    u = pow(a, r, p) * pow(c, s, p) % p
    v = pow(b, r, p) * pow(c, s, p) % p
    x = u ^ (key & 1)
    y = v ^ ((key >> 1) & 1)

    key = key >> 2

    print("x = {}".format(x))
    print("y = {}".format(y))
