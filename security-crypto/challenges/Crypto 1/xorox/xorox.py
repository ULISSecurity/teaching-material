#!/usr/bin/env python3

from secret import flag, key1, key2, key3
import os


def xor(s1, s2):
    return bytes(a ^ b for a, b in zip(s1, s2))


key2_xor_key1 = xor(key2, key1)
key2_xor_key3 = xor(key2, key3)
flag_xor_key1_xor_key2_xor_key3 = xor(flag, xor(key1, xor(key2, key3)))

print(key1.hex())
print(key2_xor_key1.hex())
print(key2_xor_key3.hex())
print(flag_xor_key1_xor_key2_xor_key3.hex())
