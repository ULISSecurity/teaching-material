from pwn import *


p = process("chal.py")

paddingChar = ""

for i in range(32,127):
    p.recvuntil("here:")

    char = chr(i)
    text = "}" + char * 31
    send_text = text+"a"


    p.sendline(send_text)

    #mangio -> Here's your encrypted text:
    p.recvline()

    #mangio -> \n
    p.recvline()

    #mangio il ciphertext
    cipher_text = p.recvline()


    #converto i bytes in stringa
    cipher_text = cipher_text.decode("utf-8")

    #rimuovo il newline finale
    cipher_text = cipher_text.strip()


    c1, c2, c3 = cipher_text[:64], cipher_text[64:128], cipher_text[128:192]


    if(c1 == c3):
        paddingChar = char
        print(f"\n\nthe padding char is: {paddingChar} - {i}\n\n")
        break



#chiudo processo
p.close()
