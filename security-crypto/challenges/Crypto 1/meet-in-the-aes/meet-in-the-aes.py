#!/usr/bin/env python3

# author: @meowmeowxw

import random
from os import urandom
from string import printable

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

from secret import flag

random.seed(urandom(32))

key1 = b"\x00" * 13 + bytes([random.choice(range(30, 80)) for _ in range(3)])
key2 = bytes([random.choice(range(30, 80)) for _ in range(3)]) + b"\x00" * 13

print(key1.hex())
print(key2.hex())
cipher1 = AES.new(key=key1, mode=AES.MODE_ECB)
cipher2 = AES.new(key=key2, mode=AES.MODE_ECB)

pt = pad(b"a" * 16, AES.block_size) # KNOWN PLAINTEXT

c1 = cipher1.encrypt(pt)
c2 = cipher2.encrypt(c1)
print(f"Encrypted string:\n {c2.hex()}")
# "d5919b33e8b8e6776a84e289850ab1d09258f2742c3241be45de931d3bfbb6d8"

flag = pad(flag, AES.block_size) # NOT KNWON
# length of flag is a multiple of 16
ct1 = cipher1.encrypt(flag)
ct2 = cipher2.encrypt(ct1)
print(f"\nEncrypted Flag:\n {ct2.hex()}")
# "b39a7b68c4dad9d434219e0a785118f3242acf2d8d06f5e700f5f6520449d64b486a5fdd3a1b3c2a99fbc9426fb0b276"
